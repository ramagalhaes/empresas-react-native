import axios from 'axios';
import React from 'react';

export const UserContext = React.createContext();

const UserContextProvider = ({children}) => {
    const [data, setData] = React.useState(null);
    const [logado, setLogado] = React.useState(false);

    const logar = React.useCallback(async (email, password) => {
        try {
            const response = await axios.post(
                'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
                {email, password},
            );
            const token = response.headers['access-token'];
            const client = response.headers.client;
            const uid = response.headers.uid;
            const dados = {
                client,
                uid,
                token,
            };
            setData(dados);
            setLogado(true);
        } catch (erro) {
            console.warn(erro);
        }
    }, []);

    return (
        <UserContext.Provider value={{data, logado, logar}}>
            {children}
        </UserContext.Provider>
    );
};

export default UserContextProvider;
