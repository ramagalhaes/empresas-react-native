import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import UserContextProvider from './utils/UserContext';
import Routes from './router/Routes';

const App = () => {
    return (
        <SafeAreaView style={styles.container}>
            <UserContextProvider>
                <Routes />
            </UserContextProvider>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fec63e',
    },
});

export default App;
