import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home/Home';
import Login from '../screens/Login/Login';
import {StyleSheet} from 'react-native';
import {UserContext} from '../utils/UserContext';
import Empresa from '../screens/Empresa/Empresa';
import Busca from '../screens/Busca/Busca';


const Stack = createStackNavigator();

const Routes = () => {
    const {logado} = React.useContext(UserContext);
    return (
        <NavigationContainer style={styles.container}>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false,
                }}
                initialRouteName="Home">
                {logado === false ? (
                    <Stack.Screen name="Login" component={Login} />
                ) : (
                    <>
                        <Stack.Screen name="Home" component={Home} />
                        <Stack.Screen name="Empresa" component={Empresa} />
                        <Stack.Screen name="Busca" component={Busca} />

                    </>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fec63e',
    },
});

export default Routes;
