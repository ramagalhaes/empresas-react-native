import React from 'react';
import {View, Text, Dimensions, StyleSheet, TouchableOpacity} from 'react-native';

const Titulo = ({title, voltar, onPress}) => {
    return (
        <View style={styles.titleContainer}>
            {voltar? (
                <TouchableOpacity style={{marginRight: 20}} onPress={onPress}>
                    <Text>voltar</Text>
                </TouchableOpacity>) : false}
            <Text style={styles.title}>{title}</Text>
        </View>
    );
};

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    titleContainer: {
        width: width - 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        fontFamily: 'helvetica',
        textTransform: 'uppercase',
        color: '#000',
    },
});

export default Titulo;
