import React from 'react';
import {TouchableOpacity, Image, View, Text, FlatList, StyleSheet, Dimensions} from 'react-native';

const Lista = ({empresas, navigation}) => {

    function renderItem({item}) {
        return (
            <TouchableOpacity style={styles.listItem} onPress={() => navigation.navigate("Empresa", item.id)}>
                <Image style={styles.img} source={{uri: `https://empresas.ioasys.com.br${item.photo}`}} />
                <View style={styles.infoContainer}>
                    <Text style={styles.title}>{item['enterprise_name']}</Text>
                    <Text style={styles.text}>{item.country}</Text>
                    <Text>USD: <Text style={styles.text}>{item['share_price']}</Text></Text>
                </View>
            </TouchableOpacity>
        );
    }


    return (
        <View style={styles.listContainer}>
                <FlatList 
                    data={empresas}
                    keyExtractor={item => item.id}
                    renderItem={renderItem}
                />
            </View>
    )
}

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    listContainer: {
        flex: 9,
        width: width - 40,
    },
    listItem: {
        height: 120,
        alignItems: 'center',
        marginBottom: 20,
        flexDirection: 'row',
        borderBottomColor: '#252525',
        borderBottomWidth: 1
    },
    infoContainer: {
        marginLeft: 10,
        height: 100,
        alignItems: 'center',
        width: 230,
        justifyContent: 'space-around',
    },
    title: {
        color: '#252525',
        fontWeight: 'bold',
        fontSize: 22,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    img: {
        width: 100,
        height: 100,
    },
});

export default Lista;
