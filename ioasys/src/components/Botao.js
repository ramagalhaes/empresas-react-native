import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

const Botao = ({texto, onPress, primary}) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={primary ? styles.btnPrimary : styles.btnSecondary}>
            <Text style={primary ? styles.textoPrimary : styles.textoSecondary}>
                {texto}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btnPrimary: {
        backgroundColor: '#252525',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 20
    },
    btnSecondary: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#252525',
        borderWidth: 2,
    },
    textoPrimary: {
        fontSize: 20,
        color: '#f3c63e',
        fontWeight: 'bold',
    },
    textoSecondary: {
        fontSize: 20,
        color: '#252525',
        fontWeight: 'bold',
    },
});

export default Botao;
