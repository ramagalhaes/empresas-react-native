import React from 'react'
import { SafeAreaView, StyleSheet, Text, View, TextInput, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';
import Botao from '../../components/Botao';
import Titulo from '../../components/Titulo';
import { UserContext } from '../../utils/UserContext';
import axios from 'axios';
import Lista from '../../components/Lista';

const Busca = ({navigation}) => {
    const {data} = React.useContext(UserContext);
    const [tipo, setTipo] = React.useState();
    const [name, setName] = React.useState();
    const [empresas, setEmpresas] = React.useState([]);

    async function fetchEmpresas() {
        try {
            const response = await axios.get(
                `https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=${tipo}&name=${name}`,
                {
                    headers: {
                        "access-token": data.token,
                        "client": data.client,
                        "uid": data.uid,
                    },
                },
            );
            setEmpresas(response.data.enterprises);
        } catch (err) {
            console.warn(err);
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <Titulo voltar onPress={() => navigation.goBack()}/>
            <View style={styles.formContainer}>
                <TextInput
                    value={tipo}
                    onChangeText={valor => setTipo(valor)}
                    style={styles.input}
                    placeholder="Tipo"
                    placeholderTextColor="#252525"
                    color="#000"
                    keyboardType="numeric"
                    maxLength={2}
                />
                <TextInput
                    value={name}
                    onChangeText={valor => setName(valor)}
                    style={styles.input}
                    placeholder="Nome"
                    placeholderTextColor="#252525"
                    color="#000"
                />
                <Botao primary texto="Buscar" onPress={fetchEmpresas}/>
            </View>
            {empresas.length? (<Lista empresas={empresas} navigation={navigation}/>) : false}
        </SafeAreaView>
    )
}

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fec63e',
        alignItems: 'center'
    },
    formContainer: {
        width: 320,
        flex: 9,
        color: '#fff',
    },
    input: {
        borderBottomColor: '#252525',
        borderBottomWidth: 1,
        height: 60,
        fontSize: 20,
    }, listContainer: {
        flex: 9,
        width: width - 40,
    },
});

export default Busca;
