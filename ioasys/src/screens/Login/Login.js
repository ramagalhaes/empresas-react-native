import React from 'react';
import {Image, SafeAreaView, StyleSheet, TextInput, View} from 'react-native';
import Botao from '../../components/Botao';
import Logo from '../../assets/logo_ioasys.png';
import {UserContext} from '../../utils/UserContext';
const Login = () => {
    const userAuth = React.useContext(UserContext);
    const [email, setEmail] = React.useState(null);
    const [password, setPassword] = React.useState(null);

    function handleLogin() {
        userAuth.logar(email, password);
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.formContainer}>
                <Image source={Logo} />
                <TextInput
                    value={email}
                    onChangeText={valor => setEmail(valor)}
                    style={styles.input}
                    placeholder="E-mail"
                    placeholderTextColor="#252525"
                    color="#000"
                />
                <TextInput
                    value={password}
                    onChangeText={valor => setPassword(valor)}
                    style={styles.input}
                    placeholder="Senha"
                    secureTextEntry={true}
                    placeholderTextColor="#252525"
                    color="#000"
                />
                <Botao primary texto="Entrar" onPress={handleLogin} />
                <Botao texto="Registre-se" />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fec63e',
    },
    formContainer: {
        width: 320,
        height: 500,
        color: '#fff',
    },
    input: {
        borderBottomColor: '#252525',
        borderBottomWidth: 1,
        height: 60,
        fontSize: 20,
    },
});

export default Login;
