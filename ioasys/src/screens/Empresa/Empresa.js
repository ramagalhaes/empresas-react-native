import React from 'react'
import { SafeAreaView, StyleSheet, Text, Dimensions, View, FlatList, TouchableOpacity, Image, ScrollView } from 'react-native';
import Titulo from '../../components/Titulo';
import axios from 'axios';
import {UserContext} from '../../utils/UserContext';


const Empresa = ({navigation, route}) => {
    const id = route.params;

    const {data} = React.useContext(UserContext);
    const[empresa, setEmpresa] = React.useState({});
    const[tipo, setTipo] = React.useState({});

    React.useEffect(() =>{
        fetchEmpresa();
    }, []);

    async function fetchEmpresa() {
        try {
            const response = await axios.get(
                `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`,
                {
                    headers: {
                        "access-token": data.token,
                        "client": data.client,
                        "uid": data.uid,
                    },
                },
            );
            setEmpresa(response.data.enterprise);
            setTipo(response.data.enterprise['enterprise_type'])
        } catch (err) {
            console.warn(err);
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <Titulo voltar onPress={() => navigation.goBack()}/>
            <View style={styles.listContainer}>
                <Image style={styles.img} source={{uri: `https://empresas.ioasys.com.br${empresa.photo}`}} />
                <Text style={styles.title}>{empresa['enterprise_name']}</Text>
                <ScrollView
                    style={styles.infoContainer}
                    contentContainerStyle={{alignItems: 'center', paddingBottom: 60}}
                 >
                    <Text>{empresa.description}</Text>
                    <View style={styles.info}>
                        <Text style={styles.head}>Country: </Text>
                        <Text style={styles.body}>{empresa.country}</Text>
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.head}>City: </Text>
                        <Text style={styles.body}>{empresa.city}</Text>
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.head}>Tipo: </Text>
                        <Text style={styles.body}>{tipo['enterprise_type_name']}</Text>
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.head}>Acoes: </Text>
                        <Text style={styles.body}>{empresa.shares}</Text>
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.head}>Preco: </Text>
                        <Text style={styles.body}>{empresa['share_price']}</Text>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fec63e',
    },
    listContainer: {
        flex: 10,
        width: width - 40,
        alignItems: 'center',
    },
    listItem: {
        height: 120,
        alignItems: 'center',
        marginBottom: 20,
        flexDirection: 'row',
    },
    infoContainer: {
        flex: 1,
        width: width - 40,
        padding: 20,
    },
    title: {
        color: '#252525',
        fontWeight: 'bold',
        fontSize: 22,
        marginTop: 20,
        marginBottom: 20,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    img: {
        width: 150,
        height: 150,
        borderRadius: 150,
        borderColor: '#fff',
        borderWidth: 2,
    },
    info: {
        flexDirection: 'row',
        height: 40,
        width: 300,
        alignItems: 'center',
    },
    head: {
        fontSize: 20
    },
    body: {
        fontWeight: 'bold',
        fontSize: 20,
        marginLeft: 10
    }
});


export default Empresa;
