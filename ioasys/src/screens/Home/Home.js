import axios from 'axios';
import React from 'react';
import {
    SafeAreaView,
    Text,
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';
import Titulo from '../../components/Titulo';
import Botao from '../../components/Botao';
import {UserContext} from '../../utils/UserContext';
import Lista from '../../components/Lista';

const Home = ({navigation}) => {
    const {data} = React.useContext(UserContext);
    const [empresas, setEmpresas] = React.useState([]);

    React.useEffect(() => {
        fetchEmpresas();
    }, []);

    async function fetchEmpresas() {
        try {
            const response = await axios.get(
                'https://empresas.ioasys.com.br/api/v1/enterprises',
                {
                    headers: {
                        "access-token": data.token,
                        "client": data.client,
                        "uid": data.uid,
                    },
                },
            );
            setEmpresas(response.data.enterprises);
        } catch (err) {
            console.warn(err);
        }
    }

    function renderItem({item}) {
        return (
            <TouchableOpacity style={styles.listItem} onPress={() => navigation.navigate("Empresa", item.id)}>
                <Image style={styles.img} source={{uri: `https://empresas.ioasys.com.br${item.photo}`}} />
                <View style={styles.infoContainer}>
                    <Text style={styles.title}>{item['enterprise_name']}</Text>
                    <Text style={styles.text}>{item.country}</Text>
                    <Text>USD: <Text style={styles.text}>{item['share_price']}</Text></Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <SafeAreaView style={styles.container}>
            <Titulo title="Empresas" />
           <Lista navigation={navigation} empresas={empresas}/>
            <View style={styles.btnContainer}>
                <Botao texto="buscar" primary onPress={() =>{navigation.navigate("Busca")}}/>
            </View>
        </SafeAreaView>
    );
};

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fec63e',
    },
    btnContainer: {
        flex: 1,
        width: width,
    }
});

export default Home;
